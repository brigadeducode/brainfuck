package fr.unice.polytech.brigadeDuCode.brainfuck.readers;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;


public class ImageReaderTest {

    ImageReader i;

    @Before
    public void init() throws IOException {

        i = new ImageReader("src/test/resources/fileTest.bmp");
    }

    @Test
    public void generateTest() throws IOException, WrongInstructionException {
        i.read();
    }
}
