package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.CellValueException;

public class Incrementation extends Instruction{
    public Incrementation(Memory memory) throws CellValueException {
        super(memory);
        if(super.memory.getPointedValue() + 128 < 255)super.memory.setPointedValue((byte)(super.memory.getPointedValue() + 1)); //Si la valeur est inférieure à 255 on l'incrémente
        else throw new CellValueException("La cellule memoire " + super.memory.getPointer() + " est a son maximum."); //Sinon on lance une erreur
    }
}
