package fr.unice.polytech.brigadeDuCode.brainfuck.readers;

import fr.unice.polytech.brigadeDuCode.brainfuck.actions.Action;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongAdressException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;

import java.io.File;
import java.io.IOException;

public abstract class Reader {
	protected File file;
	protected Action hook;
	
	public Reader(String filePath){
		this.file = new File(filePath);
	}
	
	public String read() throws IOException, WrongInstructionException, WrongInstructionException, WrongParametersNumberException, WrongAdressException {
		return null;
	}

	public void setHook(Action hook){
		this.hook = hook;
	}
}
