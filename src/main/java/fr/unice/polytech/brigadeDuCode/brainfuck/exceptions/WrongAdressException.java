package fr.unice.polytech.brigadeDuCode.brainfuck.exceptions;

public class WrongAdressException extends Exception{
    public WrongAdressException(String name){
        super("Wrong adress for the function " + name);
    }
}
