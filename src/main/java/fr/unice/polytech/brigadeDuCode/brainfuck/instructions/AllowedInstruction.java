package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

public enum AllowedInstruction {

	INCR("INCR", '+', 0xffffffff),
	DECR("DECR", '-', 0xff4b0082),
	LEFT("LEFT", '<', 0xff9400d3),
	RIGHT("RIGHT", '>', 0xff0000ff),
	IN("IN", ',', 0xffffff00),
	OUT("OUT", '.', 0xff00ff00),
	JUMP("JUMP", '[', 0xffff7f00),
	BACK("BACK", ']', 0xffff0000);
	
	String longInstruction;
	char shortInstruction;
	int hexaInstruction;
	
	AllowedInstruction(String longInstruction, char shortInstruction, int hexaInstruction){
		this.longInstruction = longInstruction;
		this.shortInstruction = shortInstruction;
		this.hexaInstruction = hexaInstruction;
	}

	public String getLongInstruction(){ //Renvoie l'instruction longue
		return this.longInstruction;
	}

	public static String getLongInstruction(String instruction){ //Renvoie l'instruction longue associée au paramètre
		AllowedInstruction[] values = values();
		for(int i = 0; i < values.length; i++){ //Pour toutes les valeurs
			if(values[i].getLongInstruction().equals(instruction))return instruction; //Si l'instruction correspond mais est déjà longue on la renvoie telle qu'elle
			if(Integer.toString(values[i].getHexaInstruction()).equals(instruction))return values[i].getLongInstruction(); //Si l'instruction correspond à une hexa, on renvoie son équivalent en version longue
			if(instruction.length() == 1 && values[i].getShortInstruction() == instruction.charAt(0))return values[i].getLongInstruction(); //Si l'instruction correspond à une version courte, on renvoie son équivalent en version longue
		}
		return null; //Si on a rien renvoyé auparavant, renvoyer null
	}
	
	char getShortInstruction(){ //Renvoie l'instruction courte
		return this.shortInstruction;
	}
	
	public static char getShortInstruction(String instruction){
		AllowedInstruction[] values = values();
		for(int i = 0; i < values.length; i++){ //Pour toutes les valeurs
			if(instruction.length() == 1 && values[i].getShortInstruction() == instruction.charAt(0))return instruction.charAt(0); //Si l'instruction correspond mais est déjà courte on la renvoie telle qu'elle
			if(Integer.toString(values[i].getHexaInstruction()).equals(instruction))return values[i].getShortInstruction(); //Si l'instruction correspond à une hexa, on renvoie son équivalent en version courte
			if(values[i].getLongInstruction().equals(instruction))return values[i].getShortInstruction(); //Si l'instruction correspond à une version longue, on renvoie son équivalent en version courte
		}
		return '\u0000';
	}
	
	int getHexaInstruction(){
		return this.hexaInstruction;
	}
	
	public static int getHexaInstruction(String instruction){
		AllowedInstruction[] values = values();
		for(int i = 0; i < values.length; i++){ //Pour toutes les valeurs
			if(Integer.toString(values[i].getHexaInstruction()).equals(instruction))return Integer.valueOf(instruction); //Si l'instruction correspond mais est déjà en hexa on la renvoie telle qu'elle
			if(values[i].getLongInstruction().equals(instruction))return values[i].getHexaInstruction(); //Si l'instruction correspond à une version longue, on renvoie son équivalent en hexa
			if(instruction.length() == 1 && values[i].getShortInstruction() == instruction.charAt(0))return values[i].getHexaInstruction(); //Si l'instruction correspond à une version courte, on renvoie son équivalent en hexa
		}
		return -1;
	}

	static boolean isAShortInstruction(char instruction){ //Vérifie si le paramètre est une instruction courte
		AllowedInstruction[] values = values();
		for(int i = 0; i < values.length; i++){ //Pour toutes les valeurs
			if(values[i].getShortInstruction() == instruction)return true; //Si l'instruction correspond à une version courte, renvoyer true
		}
		return false; //Sinon renvoyer false
	}

	public static boolean isShortInstructions(String instruction){ //Vérifie si le paramètre est une suite d'instruction courtes
		for(int i = 0; i < instruction.length(); i++){ //Pour toutes les valeurs
			if(!isAShortInstruction(instruction.charAt(i))) return false; //Si l'instruction ne correspond pas à une version courte, renvoyer false
		}
		return true; //Sinon renvoyer true
	}
	
	public static boolean isALongInstruction(String instruction){ //Vérifie si le paramètre est une instruction longue
		AllowedInstruction[] values = values();
		for(int i = 0; i < values.length; i++){ //Pour toutes les valeurs
			if(values[i].getLongInstruction().equals(instruction))return true; //Si l'instruction correspond à une version longue, renvoyer true
		}
		return false; //Sinon renvoyer false
	}
	
	static boolean isAnHexaInstruction(String instruction){ //Vérifie si le paramètre est une instruction hexa
		AllowedInstruction[] values = values();
		for(int i = 0; i < values.length; i++){ //Pour toutes les valeurs
			if(Integer.toString(values[i].getHexaInstruction()).equals(instruction)){
				return true; //Si l'instruction correspond à une hexa, renvoyer true
			}
		}
		return false; //Sinon renvoyer false
	}

	public static boolean isAllowed(String instruction){ //Verifie si le paramètre est ou contient des instructions autorisées
		if(isALongInstruction(instruction))return true; //Si l'instruction correspond à une version longue, renvoyer true
		if(isAnHexaInstruction(instruction))return true; //Si l'instruction correspond à une hexa, renvoyer true
		if(isShortInstructions(instruction))return true; //Si l'instruction correspond à une version courte, renvoyer true
		return false; //Sinon renvoyer false
	}

}
