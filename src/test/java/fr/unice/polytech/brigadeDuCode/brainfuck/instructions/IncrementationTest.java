package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.CellValueException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.security.InvalidParameterException;

import static org.junit.Assert.*;

public class IncrementationTest {
    private Memory memory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void defineContext(){
        this.memory = new Memory();
    }

    @Test
    public void incrementation() throws CellValueException {
        memory.setPointedValue((byte)-128);
        new Incrementation(memory);
        assertEquals((byte)-127, memory.getPointedValue());
    }

    @Test
    public void incrementationWithNullMemory() throws CellValueException {
        thrown.expect(InvalidParameterException.class);
        new Incrementation(null);
    }

    @Test
    public void incrementationWhenCaseIsFull() throws CellValueException {
        thrown.expect(CellValueException.class);
        thrown.expectMessage("La cellule memoire 0 est a son maximum.");
        memory.setPointedValue((byte)127);
        new Incrementation(memory);
    }
}
