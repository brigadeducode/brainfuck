package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Scanner;

public class In extends Instruction{
	private Scanner reader;
	long debut=0;
	public static long totTime=0;

	public In(Memory memory) throws IOException {
		super(memory);
		debut=System.currentTimeMillis();
		char character = ' ';
		if(System.in instanceof BufferedInputStream){
			System.out.println("Veuillez entrer un caractere : ");
			reader = new Scanner(System.in);
			character = reader.next().charAt(0);
		}
		else character = (char)System.in.read();
		this.memory.setPointedValue((byte) (character - 128)); //On met la case pointée à la valeur entrée
		totTime=System.currentTimeMillis()-debut;
	}
}