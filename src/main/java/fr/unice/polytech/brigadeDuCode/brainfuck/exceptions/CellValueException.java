package fr.unice.polytech.brigadeDuCode.brainfuck.exceptions;

public class CellValueException extends Exception{

	private static final long serialVersionUID = -7416821304568244789L;
	
	public CellValueException(String message){
		super(message);
	}
}
