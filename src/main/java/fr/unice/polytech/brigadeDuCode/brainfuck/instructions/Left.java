package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.PointerValueOutOfBoundsException;

public class Left extends Instruction{
    public Left(Memory memory) throws PointerValueOutOfBoundsException {
        super(memory);
        if(super.memory.getPointer() >= 1)super.memory.setPointer(super.memory.getPointer() - 1); //Si le pointeur est supérieur à 0, on le décrémente
        else throw new PointerValueOutOfBoundsException("Le pointeur est a son minimum."); //Sinon on lance une erreur
    }
}
