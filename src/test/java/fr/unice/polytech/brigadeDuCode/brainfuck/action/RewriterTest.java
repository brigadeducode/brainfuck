package fr.unice.polytech.brigadeDuCode.brainfuck.action;

import fr.unice.polytech.brigadeDuCode.brainfuck.actions.Rewriter;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongAdressException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.TextReader;
import org.junit.Before;
import org.junit.Test;
import java.io.*;

public class RewriterTest {

    Rewriter r;

    @Before
    public void init() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File("src/test/resources/fileTest.bf")));
        writer.write("INCR");
        writer.newLine();
        writer.write("+");
        writer.newLine();
        writer.write("DECR");
        writer.newLine();
        writer.write("-");
        writer.newLine();
        writer.write("RIGHT");
        writer.newLine();
        writer.write(">");
        writer.newLine();
        writer.write("LEFT");
        writer.newLine();
        writer.write("<");
        writer.newLine();
        writer.write("OUT");
        writer.newLine();
        writer.write(".");
        writer.newLine();
        writer.write("JUMP");
        writer.newLine();
        writer.write("[");
        writer.newLine();
        writer.write("BACK");
        writer.newLine();
        writer.write("]");
        writer.newLine();
        writer.close();

        r = new Rewriter(new TextReader("src/test/resources/fileTest.bf"));
    }

    @Test
    public void test() throws WrongAdressException, WrongInstructionException, WrongParametersNumberException, IOException {
        r.rewrite();
    }
}
