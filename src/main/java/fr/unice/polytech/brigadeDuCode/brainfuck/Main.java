package fr.unice.polytech.brigadeDuCode.brainfuck;

import fr.unice.polytech.brigadeDuCode.brainfuck.actions.*;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.*;
import fr.unice.polytech.brigadeDuCode.brainfuck.instructions.In;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.ImageReader;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.TextReader;

import java.io.*;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

public class Main {


	public static void main(String[] args) throws IOException, WrongInstructionException {

		String path = "text.bf";            //Chemin d'accés au fichier
		boolean rewriteArgument = false;    //Si l'argument "--rewrite" a été passé en paramètre
		boolean translateArgument = false;  //Si l'argument "--translate" a été passé en paramètre
		boolean checkArgument = false;      //Si l'argument "--check" a été passé en paramètre
		boolean metricsArgument = false;    //Si l'argument "--metrics" a été passé en paramètre
		boolean traceArgument = false;      // Si l'argument --trace a été passé en paramètre
		boolean generationCode = false;          // Si l'argument --generator  est passé en paramètre
		long debut = 0;
		long totTime = 0;

		if(args.length != 0){                //Si le nombre d'arguments n'est pas nul
			for(int i = 0; i < args.length; i++){     //Pour chaque argument
				switch(args[i]){
					case "-p":                   //Si l'argument est égal à "-p"
						if (i+1<args.length) path = args[i+1];
						break;

					case "--rewrite":            //Si l'argument est égal à "--rewrite"
						rewriteArgument = true;
						break;

					case "--translate":          //Si l'argument est égal à "--translate"
						translateArgument = true;
						break;

					case "--check":              //Si l'argument est égal à "--check"
						checkArgument = true;
						break;

					case "--metrics":            //Si l'argument est égal à "--rewrite"
						metricsArgument = true;
						break;

					case "--generator":
						generationCode = true;
						break;

					case "-i" :                  //Si l'argument est égal à "-i"
						if(i+1 < args.length){
							FileInputStream input;
							try{
								input = new FileInputStream(args[i+1]);
								System.setIn(input);
							} catch (FileNotFoundException e){
								System.err.println("Le fichier d'entrée n'a pas été trouvé");
								System.exit(3);
							}
						}
						break;

					case "-o" : //Si l'argument est égal à "-o"
						if (i+1<args.length) System.setOut(new PrintStream(args[i+1]));
						break;

					case "--trace" : //si l'argument est égal à --trace
						traceArgument=true;
				}
			}

			if(path != null){ //Si on a trouvé un chemin parmis les paramètres
				try {
					String fileName = path.split("\\.")[path.split("\\.").length-2]; //On récupère le nom du fichier
					String fileExtension = path.split("\\.")[path.split("\\.").length-1]; //On récupère l'extension du chemin
					if(metricsArgument){
						Metrics metrics = new Metrics();
						Interpreter interpreter = new Interpreter((fileExtension.equals("bmp"))? new ImageReader(path) : new TextReader(path), metrics, new Tracer()); //On créé un nouvel Interpreter avec le reader associé à l'extension
						debut=System.currentTimeMillis();
						interpreter.execute();
						totTime=System.currentTimeMillis()-debut- In.totTime;
						System.out.println("EXEC_TIME : "+totTime+" milliseconds");
						metrics.display();
					}
					else if(rewriteArgument){ //Si l'argument "--rewrite" a été passé en paramètre
						Rewriter rewriter = new Rewriter((fileExtension.equals("bmp"))? new ImageReader(path) : new TextReader(path));  //On créé un nouveau Rewriter avec le reader associé à l'extension
						rewriter.rewrite(); //On traduit le programme
					}

					/**/
					else if(generationCode){
						Generator generator = new Generator((fileExtension.equals("bmp"))? new ImageReader(path) : new TextReader(path),fileName);
						generator.generate();
						System.out.println("Generation code complete and saved to " + fileName + ".c");
					}
					/**/

					else if(translateArgument){ //Si l'argument "--translate" a été passé en paramètre
						Translator translator = new Translator((fileExtension.equals("bmp"))? new ImageReader(path) : new TextReader(path), fileName);
						translator.translate();
						System.out.println("\nTranslation complete and saved to " + fileName + ".bmp");
					}
					else if(checkArgument){
						Checker checker = new Checker((fileExtension.equals("bmp"))? new ImageReader(path) : new TextReader(path));
						checker.check();
					}
					else if(traceArgument){
						Tracer tracer = new Tracer();
						Interpreter interpreter = new Interpreter((fileExtension.equals("bmp"))? new ImageReader(path) : new TextReader(path), new Metrics(), tracer); //On créé un nouvel Interpreter avec le reader associé à l'extension
						interpreter.execute(); //On execute le programme
						tracer.giveLog(fileName);
					}
					else {
						Interpreter interpreter = new Interpreter((fileExtension.equals("bmp"))? new ImageReader(path) : new TextReader(path), new Metrics(), new Tracer()); //On créé un nouvel Interpreter avec le reader associé à l'extension
						interpreter.execute(); //On execute le programme
					}
				} catch (FileNotFoundException e) {
					System.err.println("Le fichier n'a pas été trouvé.");
					System.exit(-1);
				} catch (IOException e) {
					System.err.println("Le fichier n'a pas été trouvé.");
					System.exit(-1);
				} catch (CellValueException e) { //Lancé en cas d'une trop grande incrémentation ou décrémentation
					System.err.println(e.getMessage());
					System.exit(1);
				} catch (PointerValueOutOfBoundsException e) { //Lancé en cas d'une trop grande incrémentation ou décrémentation du pointeur
					System.err.println(e.getMessage());
					System.exit(2);
				} catch (WrongInstructionException e) { //Lancé en cas d'erreur de lecture d'instruction
					System.err.println(e.getMessage());
					System.exit(-1);
				} catch (LoopException e) { //Lancé en cas d'erreur de parenthèsage
					System.err.println(e.getMessage());
					System.exit(4);
				} catch (WrongParametersNumberException e) { //Lancé en cas d'erreur de paramètres
					System.err.println(e.getMessage());
					System.exit(5);
				} catch (WrongAdressException e) { //Lancé en cas d'erreur d'adresse
					System.err.println(e.getMessage());
					System.exit(6);
				}
			} else {
				System.err.println("Chemin d'accés au fichier non spécifié.");
				System.exit(-1);
			}
		}
	}

}
