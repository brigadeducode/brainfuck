package fr.unice.polytech.brigadeDuCode.brainfuck.exceptions;

public class WrongParametersNumberException extends Exception {
    public WrongParametersNumberException(String name, int numberExpected, int numberFound){
        super("Wrong number of arguments for the procedure " + name + ". Expected: " + numberExpected + ", Found: " + numberFound);
    }
}
