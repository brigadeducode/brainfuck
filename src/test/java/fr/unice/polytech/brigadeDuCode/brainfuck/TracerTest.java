package fr.unice.polytech.brigadeDuCode.brainfuck;

import fr.unice.polytech.brigadeDuCode.brainfuck.Tracer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.assertEquals;


public class TracerTest {

    Tracer tracer;
    @Before
    public void init(){
        this.tracer = new Tracer();
    }


    @Test
    public void incrExecStepNumberTest()
    {
        int n = tracer.execStepNumber.get(tracer.execStepNumber.size()-1);
        tracer.incrExecStepNumber();
        int m = tracer.execStepNumber.get(tracer.execStepNumber.size()-1);
        assertEquals(n+1,m);
    }

    @Test
    public void setLocationExecPointerTest()
    {
        tracer.setLocationExecPointer(2);
        int n = tracer.locationExecPointer.get(tracer.locationExecPointer.size()-1);
        assertEquals(n,2);
    }

    @Test
    public void incrLocationDataPointerTest()
    {
        int n = tracer.locationDataPointer.get(tracer.locationDataPointer.size()-1);
        tracer.incrLocationDataPointer();
        int m = tracer.locationDataPointer.get(tracer.locationDataPointer.size()-1);
        assertEquals(m,n+1);
    }

    @Test
    public void snapTheCaseTest()
    {
        tracer.snapTheCase(2);
        int n = tracer.snapshotMemory.get(tracer.snapshotMemory.size()-1);
        assertEquals(2+128,n);
    }

}
