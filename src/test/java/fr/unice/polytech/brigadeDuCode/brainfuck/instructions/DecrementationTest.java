package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.CellValueException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import java.security.InvalidParameterException;
import static org.junit.Assert.assertEquals;

public class DecrementationTest {
    private Memory memory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void defineContext(){
        this.memory = new Memory();
    }

    @Test
    public void decrementation() throws CellValueException {
        memory.setPointedValue((byte)-127);
        new Decrementation(memory);
        assertEquals((byte)-128, memory.getPointedValue());
    }

    @Test
    public void decrementationWithNullMemory() throws CellValueException {
        thrown.expect(InvalidParameterException.class);
        new Decrementation(null);
    }

    @Test
    public void decrementationWhenCaseIsFull() throws CellValueException {
        thrown.expect(CellValueException.class);
        thrown.expectMessage("La cellule memoire 0 est a son minimum.");
        memory.setPointedValue((byte)-128);
        new Decrementation(memory);
    }
}
