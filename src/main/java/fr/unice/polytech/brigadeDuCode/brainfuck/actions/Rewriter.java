package fr.unice.polytech.brigadeDuCode.brainfuck.actions;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongAdressException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;
import fr.unice.polytech.brigadeDuCode.brainfuck.instructions.AllowedInstruction;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.Reader;

import java.io.IOException;

public class Rewriter {
	private Reader reader;
	
	public Rewriter(Reader reader){
		this.reader = reader;
	}
	
	public void rewrite() throws IOException, WrongInstructionException, WrongParametersNumberException, WrongAdressException {
		String instruction = this.reader.read(); //On lit la première instruction
		while (instruction != null){ //Tant que l'instruction n'est pas null
			System.out.print(AllowedInstruction.getShortInstruction(instruction)); //Affiche l'équivalent de l'instruction en version courte
			instruction = this.reader.read(); //On récupère l'instruction suivante
		}
	}
}
