package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.PointerValueOutOfBoundsException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.security.InvalidParameterException;

import static org.junit.Assert.*;

public class LeftTest {
    private Memory memory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void defineContext(){
        this.memory = new Memory();
    }

    @Test
    public void left() throws PointerValueOutOfBoundsException {
        memory.setPointer(1);
        new Left(memory);
        assertEquals(0, memory.getPointer());
    }

    @Test
    public void leftWithNullMemory() throws PointerValueOutOfBoundsException {
        thrown.expect(InvalidParameterException.class);
        new Left(null);
    }

    @Test
    public void leftWhenCaseIsFull() throws PointerValueOutOfBoundsException {
        thrown.expect(PointerValueOutOfBoundsException.class);
        thrown.expectMessage("Le pointeur est a son minimum.");
        memory.setPointer(0);
        new Left(memory);
    }
}
