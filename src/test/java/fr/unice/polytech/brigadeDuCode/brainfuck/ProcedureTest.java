package fr.unice.polytech.brigadeDuCode.brainfuck;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


/**
 * Created by Théo on 04/01/2017.
 */
public class ProcedureTest {

    Procedure p;

    @Before
    public void init(){
        p = new Procedure("nom()","++");
    }

    @Test
    public void test() throws WrongParametersNumberException {
        assertEquals("nom()",p.getName());
        assertEquals("++",p.getCode("nom"));
        assertEquals("<++>",p.getCode("nom",10,11));
        assertEquals(">++<",p.getCode("nom",11,10));
    }

}
