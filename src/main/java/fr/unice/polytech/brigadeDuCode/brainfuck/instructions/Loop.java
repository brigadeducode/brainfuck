package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.Metrics;
import fr.unice.polytech.brigadeDuCode.brainfuck.Tracer;
import fr.unice.polytech.brigadeDuCode.brainfuck.actions.Interpreter;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.CellValueException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.LoopException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.PointerValueOutOfBoundsException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Loop extends Instruction {

    private boolean isReadingLoop;
    private int loopNumber;
    private List<AllowedInstruction> loopInstructions;
    private Interpreter hook;
    private Tracer tracer;
    private Metrics metrics;

    public Loop(Memory memory, List<AllowedInstruction> instructions, Interpreter hook) throws IOException, WrongInstructionException, CellValueException, PointerValueOutOfBoundsException, LoopException {
        super(memory);
        this.hook = hook;
        Tracer tracer = (this.hook == null)? new Tracer() : this.hook.getTracer();
        Metrics metrics = (this.hook == null)? new Metrics() : this.hook.getMetrics();
        this.isReadingLoop = false;
        this.loopNumber = 0;
        this.loopInstructions = new ArrayList<>();
        while(super.memory.getPointedValue() != -128){
            for (int i = 0; i < instructions.size(); i++) {
                if(this.isReadingLoop){
                    metrics.countDataRead();
                    metrics.countExecMove();
                    if(instructions.get(i).equals(AllowedInstruction.JUMP)) { //Si l'instruction correspond à un JUMP
                        this.loopNumber++;
                        this.loopInstructions.add(instructions.get(i));
                    }
                    else if(this.loopNumber != 0 && instructions.get(i).equals(AllowedInstruction.BACK)) { //Si l'instruction correspond à un BACK
                        this.loopNumber--;
                        this.loopInstructions.add(instructions.get(i));
                    }
                    else if(this.loopNumber == 0 && instructions.get(i).equals(AllowedInstruction.BACK)) { //Si l'instruction correspond à un BACK
                        new Loop(this.memory, this.loopInstructions, this.hook);
                        this.isReadingLoop = false;
                        this.loopInstructions = new ArrayList<>();
                    } else this.loopInstructions.add(instructions.get(i));
                } else {
                	tracer.incrExecStepNumber();
                    if (instructions.get(i).equals(AllowedInstruction.INCR)) {//Si l'instruction correspond à une incrémentation
                        new Incrementation(this.memory); //On incrémente la mémoire
                        tracer.snapTheCase(this.memory.getPointedValue());
                        metrics.countDataWrite();
    					metrics.countExecMove();
                    }if (instructions.get(i).equals(AllowedInstruction.DECR)){ //Si l'instruction correspond à une décrémentation
                        new Decrementation(this.memory); //On décrémente la mémoire
                        tracer.snapTheCase(this.memory.getPointedValue());
                        metrics.countDataWrite();
    					metrics.countExecMove();
                    }if (instructions.get(i).equals(AllowedInstruction.RIGHT)){ //Si l'instruction correspond à une incrémentation du pointeur
                        new Right(this.memory); //On incrémente le pointeur
                        tracer.setLocationExecPointer(this.memory.getPointer()); //on incremente le pointeur du tableau memoire pour le traçeur
						tracer.snapTheCase(this.memory.getPointedValue());
						metrics.countDataMove();
						metrics.countExecMove();
                    }if (instructions.get(i).equals(AllowedInstruction.LEFT)){ //Si l'instruction correspond à une décrémentation du pointeur
                        new Left(this.memory); //On décrémente le pointeur
						tracer.setLocationExecPointer(this.memory.getPointer());//on decremente le pointeur du tableau memoire pour le traçeur
						tracer.snapTheCase(this.memory.getPointedValue());
						metrics.countDataMove();
						metrics.countExecMove();
                	}if (instructions.get(i).equals(AllowedInstruction.OUT)){ //Si l'instruction correspond à un affichage de la valeur pointée
                        new Out(this.memory); //On affiche la valeur pointée
						tracer.snapTheCase(this.memory.getPointedValue());
						metrics.countDataRead();
						metrics.countExecMove();
            		}if (instructions.get(i).equals(AllowedInstruction.IN)){
                        new In(this.memory);
						tracer.snapTheCase(this.memory.getPointedValue());
						metrics.countExecMove();
						metrics.countDataWrite();
            		}if (instructions.get(i).equals(AllowedInstruction.JUMP)){ //Si l'instruction correspond à un JUMP
                        this.isReadingLoop = true;
						tracer.snapTheCase(this.memory.getPointedValue());
            		}if(instructions.get(i).equals(AllowedInstruction.BACK)) //Si l'instruction correspond à un BACK
                        throw new LoopException("Well-parenthesized file problem");
                }
            }
            if(isReadingLoop)throw new LoopException("Well-parenthesized file problem");
        }
    }
}
