package fr.unice.polytech.brigadeDuCode.brainfuck;

import fr.unice.polytech.brigadeDuCode.brainfuck.Metrics;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MetricsTest {

    Metrics metrics;

    @Before
    public void init()
    {
        this.metrics = new Metrics();
    }

    @Test
    public void countProgramSizeTest()
    {
        int n = metrics.cptProgramSize;
        metrics.countProgramSize();
        int m = metrics.cptProgramSize;
        assertEquals(m,n+1);
    }

    @Test
    public void countDataMoveTest()
    {
        int n = metrics.cptDataMove;
        metrics.countDataMove();
        int m = metrics.cptDataMove;
        assertEquals(n+1,m);
    }

    @Test
    public void countDataWriteTest()
    {
        int n = metrics.cptDataWrite;
        metrics.countDataWrite();
        int m = metrics.cptDataWrite;
        assertEquals(n+1,m);
    }

    @Test
    public void countDataReadTest()
    {
        int n = metrics.cptDataRead;
        metrics.countDataRead();
        int m = metrics.cptDataRead;
        assertEquals(n+1,m);
    }

    @Test
    public void countExecMoveTest()
    {
        int n = metrics.cptExecMove;
        metrics.countExecMove();
        int m = metrics.cptExecMove;
        assertEquals(n+1,m);
    }
}
