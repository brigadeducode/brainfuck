package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.InvalidParameterException;

import static org.junit.Assert.*;

public class InTest {
    private Memory memory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void defineContext(){
        this.memory = new Memory();
    }

    @Test
    public void in() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream("Ceci est un test.".getBytes());
        System.setIn(in);
        new In(memory);
        assertEquals((byte)'C', memory.getPointedValue()+128);
    }

    @Test
    public void inWithNullMemory() throws IOException {
        thrown.expect(InvalidParameterException.class);
        new In(null);
    }
}
