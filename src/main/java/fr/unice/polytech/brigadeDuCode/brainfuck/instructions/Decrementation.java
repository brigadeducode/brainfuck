package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.CellValueException;

public class Decrementation extends Instruction{
    public Decrementation(Memory memory) throws CellValueException {
        super(memory);
        if(super.memory.getPointedValue() + 128 > 0) super.memory.setPointedValue((byte)(super.memory.getPointedValue() - 1)); //Si la valeur est supérieure à 0 on la décrémente
        else throw new CellValueException("La cellule memoire " + super.memory.getPointer() + " est a son minimum."); //Sinon on lance une erreur
    }
}