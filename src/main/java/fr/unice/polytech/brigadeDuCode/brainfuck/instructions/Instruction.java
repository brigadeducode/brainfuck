package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;

import java.security.InvalidParameterException;

public abstract class Instruction {

    protected Memory memory;

    Instruction(Memory memory){
        if(memory == null)
            throw new InvalidParameterException();
        this.memory = memory;
    }
}
