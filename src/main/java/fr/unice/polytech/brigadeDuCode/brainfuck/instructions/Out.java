package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;

public class Out extends Instruction{
    public Out(Memory memory){
        super(memory);
        System.out.print((char)(super.memory.getPointedValue() + 128)); //On affiche le caractere ASCII correspondant à la valeur contenue dans le byte
    }
}
