package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.PointerValueOutOfBoundsException;

public class Right extends Instruction{
    public Right(Memory memory) throws PointerValueOutOfBoundsException {
        super(memory);
        if(super.memory.getPointer() < 3000)super.memory.setPointer(super.memory.getPointer() + 1); //Si le pointeur est inférieur à 3000, on l'incrémente
        else throw new PointerValueOutOfBoundsException("Le pointeur est a son maximum."); //Sinon on lance une erreur
    }
}
