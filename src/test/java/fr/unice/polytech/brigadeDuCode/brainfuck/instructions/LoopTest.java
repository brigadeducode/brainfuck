package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.Metrics;
import fr.unice.polytech.brigadeDuCode.brainfuck.Tracer;
import fr.unice.polytech.brigadeDuCode.brainfuck.actions.Interpreter;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.CellValueException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.LoopException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.PointerValueOutOfBoundsException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.TextReader;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LoopTest {

    Loop l;

    @Before
    public void init() throws LoopException, PointerValueOutOfBoundsException, WrongInstructionException, CellValueException, IOException {
        List<AllowedInstruction> list = new ArrayList<>();
        list.add(AllowedInstruction.INCR);
        list.add(AllowedInstruction.DECR);
        list.add(AllowedInstruction.RIGHT);
        list.add(AllowedInstruction.LEFT);
        list.add(AllowedInstruction.OUT);
        list.add(AllowedInstruction.INCR);
        list.add(AllowedInstruction.INCR);
        list.add(AllowedInstruction.JUMP);
        list.add(AllowedInstruction.DECR);
        list.add(AllowedInstruction.BACK);
        Memory m;
        m = new Memory();
        m.setPointedValue(new Byte("100"));
        l = new Loop(m,list,null);
    }

    @Test
    public void test() {
        assertEquals("","");
    }
}
