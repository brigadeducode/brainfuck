package fr.unice.polytech.brigadeDuCode.brainfuck.exceptions;

public class PointerValueOutOfBoundsException extends Exception{

	private static final long serialVersionUID = -4669396749598560202L;

	public PointerValueOutOfBoundsException(String message){
		super(message);
	}
}
