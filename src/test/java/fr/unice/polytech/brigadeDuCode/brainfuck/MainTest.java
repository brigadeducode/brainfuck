package fr.unice.polytech.brigadeDuCode.brainfuck;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import org.junit.Test;

import java.io.IOException;

public class MainTest {

    @Test
    public void testMain() throws IOException, WrongInstructionException {
        Main.main(new String[] {"-p", "src/test/resources/fileTest.bf"});
        Main.main(new String[] {"-p", "src/test/resources/fileTest.bf", "--rewrite"});
        Main.main(new String[] {"-p", "src/test/resources/fileTest.bf", "--translate"});
        Main.main(new String[] {"-p", "src/test/resources/fileTest.bf", "--generator"});
        Main.main(new String[] {"-p", "src/test/resources/fileTest.bf", "--metrics"});
        Main.main(new String[] {"-p", "src/test/resources/fileTest.bf", "--trace"});
        Main.main(new String[] {"-p", "src/test/resources/fileTest.bf", "--check"});
        Main.main(new String[] {"-p", "src/test/resources/fileTest.bf", "-i", "src/test/resources/in"});
    }
}
