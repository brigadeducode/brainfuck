package fr.unice.polytech.brigadeDuCode.brainfuck.action;

import fr.unice.polytech.brigadeDuCode.brainfuck.Metrics;
import fr.unice.polytech.brigadeDuCode.brainfuck.Tracer;
import fr.unice.polytech.brigadeDuCode.brainfuck.actions.Checker;
import fr.unice.polytech.brigadeDuCode.brainfuck.actions.Interpreter;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.*;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.TextReader;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class InterpreterTest {

    Interpreter i;

    @Before
    public void init() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File("src/test/resources/fileTest.bf")));
        writer.write("INCR");
        writer.newLine();
        writer.write("+");
        writer.newLine();
        writer.write("DECR");
        writer.newLine();
        writer.write("-");
        writer.newLine();
        writer.write("RIGHT");
        writer.newLine();
        writer.write(">");
        writer.newLine();
        writer.write("LEFT");
        writer.newLine();
        writer.write("<");
        writer.newLine();
        writer.write("OUT");
        writer.newLine();
        writer.write(".");
        writer.newLine();
        writer.write("JUMP");
        writer.newLine();
        writer.write("[");
        writer.newLine();
        writer.write("BACK");
        writer.newLine();
        writer.write("]");
        writer.newLine();
        writer.close();

        i = new Interpreter(new TextReader("src/test/resources/fileTest.bf"), new Metrics(), new Tracer());
    }

    @Test
    public void test() throws CellValueException, WrongInstructionException, IOException, LoopException, WrongParametersNumberException, PointerValueOutOfBoundsException, WrongAdressException {
        i.execute();
    }
}
