package fr.unice.polytech.brigadeDuCode.brainfuck.actions;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongAdressException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;
import fr.unice.polytech.brigadeDuCode.brainfuck.instructions.AllowedInstruction;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.Reader;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Translator {
    private Reader reader;
    private String fileName;
    private List<String> instructions;

    public Translator(Reader reader, String fileName){
        this.reader = reader;
        this.fileName = fileName;
        this.instructions = new ArrayList<>();
    }

    public void translate() throws IOException, WrongInstructionException, WrongParametersNumberException, WrongAdressException {
        String instruction = this.reader.read(); //On lit la première instruction
        while (instruction != null){ //Tant que l'instruction n'est pas null
            instructions.add(instruction);
            instruction = this.reader.read(); //On récupère l'instruction suivante
        }

        int cpt = instructions.size();
        int resolution = (int)Math.ceil(Math.pow(cpt, 0.5));
        BufferedImage bufferedImage = new BufferedImage(resolution*3,resolution*3,BufferedImage.TYPE_INT_RGB);
        int i = 0, j = 0;
        for(int k=0;k<instructions.size();k++){
            String actualInstruction = instructions.get(k);
            for (int x=i; x<i+3 ; x++) for (int y=j; y<j+3; y++) bufferedImage.setRGB(x, y, AllowedInstruction.getHexaInstruction(actualInstruction));
            if(i==resolution*3-3){
                i=0;
                j+=3;
            }
            else i+=3;
        }

        RenderedImage rendImage = bufferedImage;
        ImageIO.write(rendImage, "bmp", new File(this.fileName + ".bmp"));
    }
}
