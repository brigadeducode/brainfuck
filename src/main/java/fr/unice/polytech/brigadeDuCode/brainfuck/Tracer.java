package fr.unice.polytech.brigadeDuCode.brainfuck;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Tracer {

	public List<Integer> execStepNumber = new ArrayList<>();
	public List<Integer> locationExecPointer = new ArrayList<>();
	public List<Integer> locationDataPointer = new ArrayList<>();
	public List<Integer> snapshotMemory = new ArrayList<>();

	public Tracer()
	{
		execStepNumber.add(0);
		locationExecPointer.add(0);
		locationDataPointer.add(1);
		snapshotMemory.add(0);
	}

	public void incrExecStepNumber()
	{
		execStepNumber.add(execStepNumber.get(execStepNumber.size()-1)+1);
		locationExecPointer.add(locationExecPointer.get(locationExecPointer.size()-1));
		locationDataPointer.add(locationDataPointer.get(locationDataPointer.size()-1));
		snapshotMemory.add(snapshotMemory.get(snapshotMemory.size()-1));
	}

	public void setLocationExecPointer(int n)
	{
		locationExecPointer.add(n);
		execStepNumber.add(execStepNumber.get(execStepNumber.size()-1));
		locationDataPointer.add(locationDataPointer.get(locationDataPointer.size()-1));
		snapshotMemory.add(snapshotMemory.get(snapshotMemory.size()-1));
	}
	
	public void incrLocationDataPointer()
	{
		locationDataPointer.add(locationDataPointer.get(locationDataPointer.size()-1)+1);
		execStepNumber.add(execStepNumber.get(execStepNumber.size()-1));
		locationExecPointer.add(locationExecPointer.get(locationExecPointer.size()-1));
		snapshotMemory.add(snapshotMemory.get(snapshotMemory.size()-1));
	}

	public void snapTheCase(int n)
	{
		snapshotMemory.add(n+128);
		locationExecPointer.add(locationExecPointer.get(locationExecPointer.size()-1));
		locationDataPointer.add(locationDataPointer.get(locationDataPointer.size()-1));
		execStepNumber.add(execStepNumber.get(execStepNumber.size()-1));
	}

	public void giveLog(String fileName) throws IOException
	{
		BufferedWriter log = new BufferedWriter(new FileWriter(new File(fileName+".log")));

		try {

			for (int i=0; i<execStepNumber.size()-1; i++)
			{

				if (i>0 && !execStepNumber.get(i-1).equals(execStepNumber.get(i)))
				{
					log.write("Execution step number : " + execStepNumber.get(i) + "     Location of the execution pointer : cell: " + locationExecPointer.get(i) + "     location of the data pointer : "+ locationDataPointer.get(i)+"     Valeur de la case mémoire pointée : "+ snapshotMemory.get(i+1));
					log.newLine();
				}
			}

			log.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
