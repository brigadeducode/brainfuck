package fr.unice.polytech.brigadeDuCode.brainfuck;

import java.util.Arrays;

public class Memory {
	private byte[] tab;
	private int pointer;

	public Memory(){
		this.tab = new byte[30000];
		Arrays.fill(this.tab, (byte)-128); //On remplit le tableau avec sa valeur minimale (-128)
		this.pointer = 0;
	}

	public void setPointedValue(byte value){
		this.tab[this.pointer] = value;
	}

	public byte getPointedValue(){
		return this.tab[this.pointer];
	}

	public int getPointer(){ //Renvoie la valeur actuelle du pointeur
		return this.pointer;
	}

	public void setPointer(int value){
		this.pointer = value;
	}

	public void displayUsedCellValue(){ //Affiche la valeur des cases utilisées
		System.out.println("");
		for(int i = 0; i < this.tab.length; i++){
			if(this.tab[i] + 128 != 0) System.out.println("C" + i + ": " + (this.tab[i] + 128));
		}
	}
}
