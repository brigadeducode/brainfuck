# BRAINFUCK PROJECT

## Team Information

* Team name: Brigade Du Code
* Members:
    * [Romain Canovas](cr611228@etu.unice.fr)
    * [Théo Cholley](ct611996@etu.unice.fr)
    * [Gregory Merlet](mg403547@etu.unice.fr)
    * [Gaulthier Toussaint](tg300897@etu.unice.fr)
        
## Construction Project

You can already run the project :

- Open a terminal and navigate to the project folder
- Check if bfck.sh is executable, if not `chmod 755 bfck.sh`
- Create a file with brainfuck instructions like `file.bf`
- You can now run for example : `./bfck -p file.bf`

If you want to create an new executable :

- Open the project with your favorite IDE
- Use the command maven : `install`
- Open a terminal and navigate to the project folder
- Copy the file ./target/bfck-1.0.jar and replace it into the project folder
- Check if bfck.sh is executable, if not `chmod 755 bfck.sh`
- Create a file with brainfuck instructions like `file.bf`
- You can now run for example : `./bfck -p file.bf`