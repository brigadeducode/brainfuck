package fr.unice.polytech.brigadeDuCode.brainfuck.exceptions;

public class LoopException extends Exception{
	
	private static final long serialVersionUID = 7273165735107132978L;

	public LoopException(String message){
		super(message);
	}

}
