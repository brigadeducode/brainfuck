package fr.unice.polytech.brigadeDuCode.brainfuck;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.CellValueException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.LoopException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.PointerValueOutOfBoundsException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;

import java.io.IOException;
import java.util.ArrayList;

public class Metrics {

	public int cptProgramSize;
	public int cptDataMove;
	public int cptDataWrite;
	public int cptDataRead;
	public int cptExecMove;

	public Metrics(){
		cptProgramSize=0;
		cptDataMove=0;
		cptDataWrite=0;
		cptDataRead=0;
		cptExecMove=0;
	}

	public void countProgramSize(){
		cptProgramSize++;
	}
	public void countDataMove(){
		cptDataMove++;
	}
	public void countDataWrite(){
		cptDataWrite++;
	}
	public void countDataRead(){
		cptDataRead++;
	}
	public void countExecMove(){
		cptExecMove++;
	}

	protected ArrayList<Integer> listMetrics() throws WrongInstructionException, IOException, CellValueException, PointerValueOutOfBoundsException, LoopException {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(cptProgramSize);
		list.add(cptDataMove);
		list.add(cptDataWrite);
		list.add(cptDataRead);
		list.add(cptExecMove);
		return list;
	}

	public void display() throws WrongInstructionException, IOException, CellValueException, PointerValueOutOfBoundsException, LoopException{
		ArrayList<Integer> list = listMetrics();
		System.out.println("PROG_SIZE : "+list.get(0));
		System.out.println("EXEC_MOVE : "+list.get(4));
		System.out.println("DATA_MOVE : "+list.get(1));
		System.out.println("DATA_WRITE : "+list.get(2));
		System.out.println("DATA_READ : "+list.get(3));
	}

}

