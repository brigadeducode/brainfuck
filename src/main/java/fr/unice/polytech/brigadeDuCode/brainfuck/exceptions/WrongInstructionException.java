package fr.unice.polytech.brigadeDuCode.brainfuck.exceptions;

public class WrongInstructionException extends Exception {

	private static final long serialVersionUID = -1938624906529740085L;

	public WrongInstructionException(String wrongInstruction){
		super("Wrong instruction found : " + wrongInstruction);
	}
}
