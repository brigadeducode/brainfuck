package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.security.InvalidParameterException;

import static org.junit.Assert.assertEquals;

public class OutTest {
    private Memory memory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void defineContext(){
        this.memory = new Memory();
    }

    @Test
    public void out() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        memory.setPointedValue((byte)('C'-128));
        new Out(memory);
        assertEquals("C", out.toString());
    }

    @Test
    public void inWithNullMemory() throws IOException {
        thrown.expect(InvalidParameterException.class);
        new Out(null);
    }
}
