package fr.unice.polytech.brigadeDuCode.brainfuck;

/**
 * Classe qui permet de définir une macro, ainsi un nom par exemple TO_DIGIT correspond à une liste d'instructions
 */
public class Macros {

    private String name;
    private String code;
    private int repetitionNb;
    private int startIndex;

    /**
     * Constructeur normal
     * @param name le nom de la macro
     * @param code le code en SHORT correspondant à la macro
     */
    public Macros(String name, String code){
        this(name, code, 1, 0);
    }

    /**
     * Constructeur normal
     * @param name le nom de la macro
     * @param code le code en SHORT correspondant à la macro
     * @param repetitionNb le nombre de répétitons du code à éxécuter
     */
    public Macros(String name, String code, int repetitionNb){
        this(name, code, repetitionNb, 0);
    }

    /**
     * Constructeur normal
     * @param name le nom de la macro
     * @param code le code en SHORT correspondant à la macro
     * @param repetitionNb le nombre de répétitons du code à éxécuter
     * @param startIndex l'index de début d'éxécution
     */
    public Macros(String name, String code, int repetitionNb, int startIndex){
        this.name = name;
        this.code = code;
        this.repetitionNb = repetitionNb;
        this.startIndex = startIndex;
    }

    /**
     * Renvoie le nom de la macro
     * @return le nom
     */
    public String getName(){
        return this.name;
    }

    /**
     * Renvoie le code en SHORT correspondant à une macro
     * @return
     */
    public String getCode(){
        return this.getCode(this.repetitionNb, this.startIndex);
    }

    /**
     * Renvoie le code en SHORT correspondant à une macro
     * @return
     */
    public String getCode(int repetitionNb){
        return this.getCode(repetitionNb, this.startIndex);
    }

    /**
     * Renvoie le code en SHORT correspondant à une macro
     * @return
     */
    public String getCode(int repetitionNb, int startIndex){
        String returnValue = "";
        for(int i = 0; i < repetitionNb; i++){
            returnValue += this.code.substring(startIndex);
        }
        return returnValue;
    }
}
