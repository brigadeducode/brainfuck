package fr.unice.polytech.brigadeDuCode.brainfuck.actions;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.*;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.Reader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Generator {

    private Reader reader;
    private String fileName;
    private ArrayList<String> tabVariables;
    private int[] tabValeursVariables;
    private int currentNumVariable;
    private int inLoopNumber;

    public Generator(Reader reader, String fileName){
        this.reader = reader;
        this.fileName = fileName;
        tabVariables = new ArrayList<>();
        tabValeursVariables = new int[30000];
        currentNumVariable = 0;
        inLoopNumber = 0;
    }

    public void generate() throws IOException, WrongInstructionException, PointerValueOutOfBoundsException, CellValueException, WrongParametersNumberException, WrongAdressException {

        BufferedWriter cFile = new BufferedWriter(new FileWriter(new File(fileName+".c")));
        setHeaderOnFile(cFile);

        String instruction = this.reader.read();
        while (instruction != null){

            if(instruction.equals("INCR") || instruction.equals("+")){
                tabulation(cFile);
                if(!tabVariables.contains("i"+currentNumVariable)){
                    addVariable();
                    incrVariable(currentNumVariable);
                    cFile.write("    var["+currentNumVariable+"]++;");
                    cFile.newLine();
                }
                else{
                    incrVariable(currentNumVariable);
                    /*if(tabValeursVariables[currentNumVariable]>250){
                        throw new CellValueException("251");
                    }*/
                    cFile.write("    var["+currentNumVariable+"]++;");
                    cFile.newLine();
                }
            }

            else if(instruction.equals("DECR") || instruction.equals("-")){
                tabulation(cFile);
                /*if(!tabVariables.contains("i"+currentNumVariable)){
                    throw new CellValueException("-1");
                }*/
                cFile.write("    var["+currentNumVariable+"]--;");
                cFile.newLine();
            }

            else if(instruction.equals("RIGHT") || instruction.equals(">")){
                currentNumVariable++;
            }

            else if(instruction.equals("LEFT") || instruction.equals("<")){
                currentNumVariable--;
            }

            else if(instruction.equals("IN") || instruction.equals(",")){
                tabulation(cFile);
                if(!tabVariables.contains("i"+currentNumVariable)){
                    addVariable();
                }
                cFile.write("    scanf(\"%d\", &var["+currentNumVariable+"]);");
                cFile.newLine();
            }

            else if(instruction.equals("OUT") || instruction.equals(".")){
                tabulation(cFile);
                if(!tabVariables.contains("i"+currentNumVariable)){
                    addVariable();
                }
                cFile.write("    printf(\"%c\", var["+currentNumVariable+"]);");
                cFile.newLine();
            }

            else if(instruction.equals("JUMP") || instruction.equals("[")){
                cFile.newLine();
                tabulation(cFile);
                inLoopNumber++;
                cFile.write("    while(var["+currentNumVariable+"] != 0){");
                cFile.newLine();
            }

            else if(instruction.equals("BACK") || instruction.equals("]")){
                inLoopNumber--;
                tabulation(cFile);
                cFile.write("    }");
                cFile.newLine();
                cFile.newLine();
            }

            instruction = this.reader.read();
        }
        setEndOnFile(cFile);
        cFile.close();
    }

    public void incrVariable(int currentNumVariable){
        tabValeursVariables[currentNumVariable]++;
    }

    public void addVariable() throws IOException {
        tabVariables.add("i"+currentNumVariable);
    }

    public void setHeaderOnFile(BufferedWriter cFile) throws IOException {
        cFile.write("#include <stdio.h>");
        cFile.newLine();
        cFile.newLine();
        cFile.write("int main() {");
        cFile.newLine();
        cFile.newLine();
        tabulation(cFile);
        cFile.write("    int var[3000] = {0};");
        cFile.newLine();
        cFile.newLine();
    }

    public void setEndOnFile(BufferedWriter cFile) throws IOException {
        cFile.newLine();
        cFile.write("}");
    }

    public void tabulation(BufferedWriter cFile) throws IOException {
        for(int i = 0 ; i<inLoopNumber ; i++){
            cFile.write("    ");
        }
    }
}
