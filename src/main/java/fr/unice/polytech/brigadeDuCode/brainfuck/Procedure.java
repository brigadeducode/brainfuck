package fr.unice.polytech.brigadeDuCode.brainfuck;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Classe qui permet de définir une procédure, ainsi un nom par exemple toDigit correspond à une liste d'instructions
 */
public class Procedure {

    private String name;
    private String code;
    private ArrayList<String> params;
    private ArrayList<String> paramsValues;

    /**
     * Constructeur normal
     * @param name le nom de la procédure
     * @param code le code en SHORT correspondant à la procédure
     */
    public Procedure(String name, String code){
        this.name = name;
        this.code = code;
        this.params = new ArrayList<>();
        this.paramsValues = new ArrayList<>();
        String parameters = name.split("\\(")[1].replace(")", "");
        String[] parametersTab = parameters.split(",");
        for(int i = 0; i < parametersTab.length; i++){
            this.params.add(parametersTab[i]);
        }
    }

    /**
     * Renvoie le nom de la procédure
     * @return le nom
     */
    public String getName(){
        return this.name;
    }

    /**
     * Renvoie le code en SHORT correspondant à une procédure
     * @return le code
     */
    public String getCode(String name) throws WrongParametersNumberException {
        String resultCode = this.code;
        String proc = name.split("\\)")[0];
        String params = (proc.split("\\(").length > 1)? proc.split("\\(")[1]: "";
        String[] paramsValues = params.split(",");
        for(int i = 0; i < paramsValues.length; i++){
            this.paramsValues.add(paramsValues[i]);
        }
        if(this.params.size() != this.paramsValues.size())
            throw new WrongParametersNumberException(this.name, this.params.size(), this.paramsValues.size());
        for(int i = 0; i < this.params.size(); i++){
            resultCode = resultCode.replace(this.params.get(i), String.valueOf(this.paramsValues.get(i)));
        }
        this.paramsValues.clear();
        return resultCode;
    }

    /**
     * Renvoie le code en SHORT correspondant à une procédure
     * @return le code
     */
    public String getCode(String name, int adress, int actualAdress) throws WrongParametersNumberException {
        String resultCode = this.code;
        int diference = Math.abs(actualAdress - adress);
        for(int i = 0; i < diference; i++){
            if(adress < actualAdress){
                resultCode = "<" + resultCode;
            } else {
                resultCode = ">" + resultCode;
            }
        }
        String proc = name.split("\\)")[0];
        String params = (proc.split("\\(").length > 1)? proc.split("\\(")[1]: "";
        String[] paramsValues = params.split(",");
        for(int i = 0; i < paramsValues.length; i++){
            this.paramsValues.add(paramsValues[i]);
        }
        if(this.params.size() != this.paramsValues.size())
            throw new WrongParametersNumberException(this.name, this.params.size(), this.paramsValues.size());
        for(int i = 0; i < this.params.size(); i++){
            resultCode = resultCode.replace(this.params.get(i), String.valueOf(this.paramsValues.get(i)));
        }
        this.paramsValues.clear();
        for(int i = 0; i < diference; i++){
            if(adress < actualAdress){
                resultCode = resultCode + ">";
            } else {
                resultCode = resultCode + "<";
            }
        }
        return resultCode;
    }
}
