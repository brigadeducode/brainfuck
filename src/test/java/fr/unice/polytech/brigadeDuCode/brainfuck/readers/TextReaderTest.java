package fr.unice.polytech.brigadeDuCode.brainfuck.readers;

import fr.unice.polytech.brigadeDuCode.brainfuck.actions.Interpreter;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongAdressException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TextReaderTest {

    TextReader t;

    @Before
    public void init() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File("src/test/resources/fileTest2.bf")));
        writer.write("\\PROC MACRO() ++");
        writer.newLine();
        writer.write("\\PROC MACRO(x) x++");
        writer.newLine();
        writer.write("\\DEFINE MACRO3 2 1 +++");
        writer.newLine();
        writer.write("\\DEFINE MACRO2 2 +++");
        writer.newLine();
        writer.write("\\DEFINE MACRO ++");
        writer.newLine();
        writer.write("#commentaires");
        writer.newLine();

        writer.write("MACRO3");
        writer.newLine();
        writer.write("MACRO2");
        writer.newLine();
        writer.write("MACRO");
        writer.newLine();
        writer.write("MACRO()");
        writer.newLine();
        writer.write("MACRO(x)");
        writer.newLine();


        writer.write("INCR");
        writer.newLine();
        writer.write("+");
        writer.newLine();
        writer.write("DECR");
        writer.newLine();
        writer.write("-");
        writer.newLine();
        writer.write("RIGHT");
        writer.newLine();
        writer.write(">");
        writer.newLine();
        writer.write("LEFT");
        writer.newLine();
        writer.write("<");
        writer.newLine();
        writer.write("OUT");
        writer.newLine();
        writer.write(".");
        writer.newLine();
        writer.write("JUMP");
        writer.newLine();
        writer.write("[");
        writer.newLine();
        writer.write("BACK");
        writer.newLine();
        writer.write("]");
        writer.newLine();
        writer.close();

        t = new TextReader("src/test/resources/fileTest2.bf");
    }

    @Test
    public void generateTest() throws WrongAdressException, WrongInstructionException, WrongParametersNumberException, IOException {
        t.read();
    }
}
