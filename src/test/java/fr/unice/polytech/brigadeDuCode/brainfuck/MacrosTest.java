package fr.unice.polytech.brigadeDuCode.brainfuck;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MacrosTest {

    Macros m;
    Macros m2;
    Macros m3;

    @Before
    public void init(){
        m = new Macros("macro","+++");
        m2 = new Macros("macro2","++",2);
        m3 = new Macros("macro3","++",2,1);

    }

    @Test
    public void generateTest(){
        assertEquals("+++",m.getCode());
        assertEquals("macro",m.getName());

        assertEquals("++++",m2.getCode());
        assertEquals("macro2",m2.getName());

        assertEquals("++",m3.getCode(2));
        assertEquals("macro3",m3.getName());
    }


}
