package fr.unice.polytech.brigadeDuCode.brainfuck;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MemoryTest {

    Memory m;

    @Before
    public void init(){
        m = new Memory();
    }

    @Test
    public void generateTest(){
        m.setPointer(2);
        assertEquals(2,m.getPointer());
        assertEquals(-128,m.getPointedValue());
    }
}
