package fr.unice.polytech.brigadeDuCode.brainfuck.actions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;

public class Action {
    protected Memory memory;

    public void setMemory(Memory memory){
        this.memory = memory;
    }

    public Memory getMemory(){
        return this.memory;
    }
}
