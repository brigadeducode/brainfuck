package fr.unice.polytech.brigadeDuCode.brainfuck.actions;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.LoopException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongAdressException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;
import fr.unice.polytech.brigadeDuCode.brainfuck.instructions.AllowedInstruction;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.Reader;

import java.io.IOException;

public class Checker {

	private Reader reader;
	private int cpt;
	private boolean flag=true;
	
	public Checker(Reader reader){
		this.reader = reader;
	}

	public void check() throws LoopException, IOException, WrongInstructionException, WrongParametersNumberException, WrongAdressException {
		String instruction = this.reader.read();
		while (instruction != null){
			if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.JUMP.getLongInstruction())) //Si l'instruction correspond à un JUMP
				cpt++;
			else if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.BACK.getLongInstruction())){ //Si l'instruction correspond à un BACK
				cpt--;
				if (cpt<0) flag=false;
			}
			instruction = this.reader.read(); //On récupère l'instruction suivante
		}
		if (cpt!=0 || flag!=true) throw new LoopException("Well-parenthesized file problem");
	}
}