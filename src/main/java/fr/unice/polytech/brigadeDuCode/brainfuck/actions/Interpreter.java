package fr.unice.polytech.brigadeDuCode.brainfuck.actions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.Metrics;
import fr.unice.polytech.brigadeDuCode.brainfuck.Tracer;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.*;
import fr.unice.polytech.brigadeDuCode.brainfuck.instructions.*;
import fr.unice.polytech.brigadeDuCode.brainfuck.readers.Reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Interpreter extends Action{
	private Reader reader;
	private boolean isReadingLoop;
	private int loopNumber;
	private List<AllowedInstruction> loopInstructions;
	public Tracer tracer;
	public Metrics metrics;

	public Interpreter(Reader reader, Metrics metrics, Tracer tracer){
		this.memory = new Memory();
		this.reader = reader;
		this.reader.setHook(this);
		this.isReadingLoop = false;
		this.loopNumber = 0;
		this.loopInstructions = new ArrayList<>();
		this.tracer = tracer;
		this.metrics = metrics;
	}

	public Tracer getTracer(){
		return this.tracer;
	}

	public Metrics getMetrics(){
		return this.metrics;
	}
	public void execute() throws WrongInstructionException, IOException, CellValueException, PointerValueOutOfBoundsException, LoopException, WrongParametersNumberException, WrongAdressException {
		String instruction = this.reader.read(); //On lit la première instruction
		while (instruction != null){ //Tant que l'instruction n'est pas null
			metrics.countProgramSize();
			if(this.isReadingLoop){
				tracer.incrExecStepNumber(); //on incremente de nombre d'action faites pour le traçeur
				metrics.countDataRead();
				metrics.countExecMove();
				if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.JUMP.getLongInstruction())) { //Si l'instruction correspond à un JUMP
					this.loopNumber++;
					this.loopInstructions.add(AllowedInstruction.valueOf(AllowedInstruction.getLongInstruction(instruction)));
				}
				else if(this.loopNumber != 0 && AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.BACK.getLongInstruction())) { //Si l'instruction correspond à un BACK
					this.loopNumber--;
					this.loopInstructions.add(AllowedInstruction.valueOf(AllowedInstruction.getLongInstruction(instruction)));
				}
				else if(this.loopNumber == 0 && AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.BACK.getLongInstruction())) { //Si l'instruction correspond à un BACK
					new Loop(this.memory, this.loopInstructions, this);
					this.isReadingLoop = false;
					this.loopInstructions = new ArrayList<>();
				} else this.loopInstructions.add(AllowedInstruction.valueOf(AllowedInstruction.getLongInstruction(instruction)));
			} else {
				tracer.incrExecStepNumber(); //on incremente de nombre d'action faites pour le traçeur
				if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.INCR.getLongInstruction())){ //Si l'instruction correspond à une incrémentation
					new Incrementation(this.memory); //On incrémente la mémoire
					metrics.countDataWrite();
					metrics.countExecMove();
					tracer.snapTheCase(this.memory.getPointedValue());
				}if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.DECR.getLongInstruction())){ //Si l'instruction correspond à une décrémentation
					new Decrementation(this.memory); //On décrémente la mémoire
					metrics.countDataWrite();
					metrics.countExecMove();
					tracer.snapTheCase(this.memory.getPointedValue());
				}if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.RIGHT.getLongInstruction())){ //Si l'instruction correspond à une incrémentation du pointeur
					new Right(this.memory); //On incrémente le pointeur
					metrics.countDataMove();
					metrics.countExecMove();
					tracer.setLocationExecPointer(this.memory.getPointer()); //on incremente le pointeur du tableau memoire pour le traçeur
					tracer.snapTheCase(this.memory.getPointedValue());
				}
				if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.LEFT.getLongInstruction())){ //Si l'instruction correspond à une décrémentation du pointeur
					new Left(this.memory); //On décrémente le pointeur
					metrics.countDataMove();
					metrics.countExecMove();
					tracer.setLocationExecPointer(this.memory.getPointer());//on decremente le pointeur du tableau memoire pour le traçeur
					tracer.snapTheCase(this.memory.getPointedValue());
				}
				if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.OUT.getLongInstruction())){ //Si l'instruction correspond à un affichage de la valeur pointée
					new Out(this.memory); //On affiche la valeur pointée
					metrics.countDataRead();
					metrics.countExecMove();
					tracer.snapTheCase(this.memory.getPointedValue());
				}if (AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.IN.getLongInstruction())){
					new In(this.memory);
					metrics.countExecMove();
					metrics.countDataWrite();
					tracer.snapTheCase(this.memory.getPointedValue());
				}if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.JUMP.getLongInstruction())){ //Si l'instruction correspond à un JUMP
					this.isReadingLoop = true;
					tracer.snapTheCase(this.memory.getPointedValue());
				}if(AllowedInstruction.getLongInstruction(instruction).equals(AllowedInstruction.BACK.getLongInstruction())) //Si l'instruction correspond à un BACK
					throw new LoopException("Well-parenthesized file problem");
			}
			instruction = this.reader.read(); //On récupère l'instruction suivante
			tracer.incrLocationDataPointer(); //on incremente le pointeur de lecture du fichier .bf pour le traçeur
		}
		this.memory.displayUsedCellValue(); //On affiche la valeur des cases utilisées
	}
}