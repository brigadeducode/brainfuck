package fr.unice.polytech.brigadeDuCode.brainfuck.instructions;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.PointerValueOutOfBoundsException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.security.InvalidParameterException;

import static org.junit.Assert.*;

public class RightTest {
    private Memory memory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void defineContext(){
        this.memory = new Memory();
    }

    @Test
    public void right() throws PointerValueOutOfBoundsException {
        memory.setPointer(0);
        new Right(memory);
        assertEquals(1, memory.getPointer());
    }

    @Test
    public void rightWithNullMemory() throws PointerValueOutOfBoundsException {
        thrown.expect(InvalidParameterException.class);
        new Right(null);
    }

    @Test
    public void rightWhenCaseIsFull() throws PointerValueOutOfBoundsException {
        thrown.expect(PointerValueOutOfBoundsException.class);
        thrown.expectMessage("Le pointeur est a son maximum.");
        memory.setPointer(29999);
        new Right(memory);
    }
}
