package fr.unice.polytech.brigadeDuCode.brainfuck.readers;

import fr.unice.polytech.brigadeDuCode.brainfuck.Macros;
import fr.unice.polytech.brigadeDuCode.brainfuck.Procedure;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongAdressException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongParametersNumberException;
import fr.unice.polytech.brigadeDuCode.brainfuck.instructions.AllowedInstruction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TextReader extends Reader{

	private FileReader fileReader;
	private BufferedReader bufferedReader; //Buffer sur le fichier
	private String actualLine; //Ligne en cours de lecture
	private int actualCharacterIndex; //Position du dernier caractere lu
	private ArrayList<Macros> macros;
	private ArrayList<Procedure> procs;

	public TextReader(String filePath) throws FileNotFoundException {
		super(filePath);
		this.fileReader = new FileReader(super.file);
		this.bufferedReader = new BufferedReader(fileReader);
		this.actualLine = null;
		this.actualCharacterIndex = -1;
		this.macros = new ArrayList<>();
		procs = new ArrayList<>();
	}

	@Override
	public String read() throws IOException, WrongInstructionException, WrongParametersNumberException, WrongAdressException {
		if(this.actualCharacterIndex == -1){
			this.actualLine = this.bufferedReader.readLine(); //Si il n'y a pas de ligne en cours, lire une nouvelle ligne
			if (this.actualLine != null && this.actualLine.length() > 0){
				if (this.actualLine.contains("#")){
					String[] tabSplit = this.actualLine.split("#");
					if(tabSplit.length > 0) this.actualLine = tabSplit[0];
				}
				if(this.actualLine.contains("\\DEFINE")){
					String[] tab_macro = this.actualLine.split(" ");
					if(tab_macro.length == 3) macros.add(new Macros(tab_macro[1],replaceMacrosAndProcs(tab_macro[2])));
					else if(tab_macro.length == 4) macros.add(new Macros(tab_macro[1],replaceMacrosAndProcs(tab_macro[3]), Integer.valueOf(tab_macro[2])));
					else if(tab_macro.length == 5) macros.add(new Macros(tab_macro[1],replaceMacrosAndProcs(tab_macro[4]), Integer.valueOf(tab_macro[2]), Integer.valueOf(tab_macro[3])));
					this.actualLine = "";
				}
				if (this.actualLine.contains("\\PROC")){
					String[] tab_proc = this.actualLine.split(" ");
					procs.add(new Procedure(tab_proc[1], replaceMacrosAndProcs(tab_proc[2])));
					this.actualLine = "";
				}
				this.actualLine = replaceMacrosAndProcs(this.actualLine);
				if (this.actualLine.contains("\t")) this.actualLine = this.actualLine.replace("\t", "");
				if (this.actualLine.contains(" ")) this.actualLine = this.actualLine.replace(" ", "");
			}
		}
		else{
			if(this.actualCharacterIndex != this.actualLine.length()-1){ //Si le dernier caractere lu n'était pas le dernier de la ligne
				this.actualCharacterIndex++;
				return Character.toString(this.actualLine.charAt(this.actualCharacterIndex)); //Et on le retourne
			} else {

				this.actualCharacterIndex = -1; //On remet la position du dernier caractere lu à -1 pour signifier que la ligne est finie
				return read(); //On passe à la ligne suivante
			}
		}
		if(this.actualLine != null && this.actualLine.equals("")) {
			return read();
		}
		if((this.actualLine != null && AllowedInstruction.isAllowed(this.actualLine))){ //Si la ligne actuelle n'est pas nulle et est une instruction valide
			if(AllowedInstruction.isALongInstruction(this.actualLine)) return this.actualLine; //Si la ligne actuelle est une instruction longue, on la renvoie
			if(AllowedInstruction.isShortInstructions(this.actualLine)){ //Si la ligne actuelle contient des instructions courtes
				this.actualCharacterIndex = 0; //On prends le premier caractere
				return Character.toString(this.actualLine.charAt(this.actualCharacterIndex)); //Et on le retourne
			}
		}
		if(this.actualLine != null && !AllowedInstruction.isAllowed(this.actualLine))throw new WrongInstructionException(this.actualLine); //Si la ligne actuelle n'est pas nulle et n'est pas valide on lance une erreur de lecture d'instruction
		return null; //Fin du fichier
	}

	private String replaceMacrosAndProcs(String line) throws WrongParametersNumberException, WrongAdressException {
		String[] tab_line = line.split(" ");
		for(Macros m : this.macros)
			if (removeValidInstructions(tab_line[0]).equals(removeValidInstructions(m.getName()))) {
				if (tab_line.length == 1)
					line = tab_line[0].replace(m.getName(), m.getCode());
				if (tab_line.length == 2)
					line = tab_line[0].replace(m.getName(), m.getCode(Integer.valueOf(tab_line[1])));
				if (tab_line.length == 3)
					line = tab_line[0].replace(m.getName(), m.getCode(Integer.valueOf(tab_line[1]), Integer.valueOf(tab_line[2])));
			}
			for(Procedure p : this.procs) {
				if (removeValidInstructions(tab_line[0]).split("\\(")[0].equals(removeValidInstructions(p.getName()).split("\\(")[0])) {
					String[] adressTab = tab_line[0].split("->");
					int adress = -1;
					if(adressTab.length > 1){
						adressTab[1] = removeValidInstructions(adressTab[1]);
						if(adressTab[1].matches("^-?\\d+$")){
							adress = Integer.parseInt(adressTab[1]);
							tab_line[0] = tab_line[0].replace("->" + adress, "");
						} else throw new WrongAdressException(adressTab[0]);
					}
					if(adress == -1){
						line = tab_line[0].replace(tab_line[0].split("\\)")[0] + ")", p.getCode(line));
					} else {
						line = tab_line[0].replace(tab_line[0].split("\\)")[0] + ")", p.getCode(line, adress, this.hook.getMemory().getPointer()));
					}
				}
			}
		return line;
	}

	private String removeValidInstructions(String line){
		String resultLine = line;
		for(int i = 0; i < resultLine.length(); i++){
			if(AllowedInstruction.isAllowed(String.valueOf(resultLine.charAt(i))))
				resultLine = resultLine.replace(String.valueOf(resultLine.charAt(i)), "");
		}
		return resultLine;
	}
}
