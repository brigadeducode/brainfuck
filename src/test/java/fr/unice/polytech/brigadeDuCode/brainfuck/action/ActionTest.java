package fr.unice.polytech.brigadeDuCode.brainfuck.action;

import fr.unice.polytech.brigadeDuCode.brainfuck.Memory;
import fr.unice.polytech.brigadeDuCode.brainfuck.actions.Action;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ActionTest {

    Action a;

    @Before
    public void init(){
        a = new Action();
    }

    @Test
    public void test(){
        Memory m = new Memory();
        a.setMemory(m);
        assertEquals(m,a.getMemory());
    }
}
