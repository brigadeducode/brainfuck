package fr.unice.polytech.brigadeDuCode.brainfuck.readers;

import fr.unice.polytech.brigadeDuCode.brainfuck.exceptions.WrongInstructionException;
import fr.unice.polytech.brigadeDuCode.brainfuck.instructions.AllowedInstruction;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageReader extends Reader{

	private BufferedImage bufferedImage; //Buffer sur l'image
	private int actualX; //Position en abscisse du pixel actuel
	private int actualY; //Position en ordonée du pixel actuel
	
	public ImageReader(String filePath) throws IOException {
		super(filePath);
		this.bufferedImage = ImageIO.read(super.file);
		this.actualX = 0;
		this.actualY = 0;
	}

	@Override
	public String read() throws IOException, WrongInstructionException {
		String returnString = null;
		int pixelColor = bufferedImage.getRGB(this.actualX, this.actualY); //On charge la couleur du pixel dans la variable pixelColor
		returnString = Integer.toString(pixelColor);
		this.actualX += 3; //On passe au pixel suivant à étudier
		if(this.actualX > this.bufferedImage.getWidth()-1){ //Si on a dépassé la taille de l'image en abscisse
			this.actualX = 0; //On remet la position en abscisse à 0
			this.actualY += 3; //On passe à la ligne suivante à étudier
		}
		if(this.actualY > this.bufferedImage.getHeight()-1) return null; //Si on a dépassé la taille de l'image en ordonnée, on retourne null pour signifier la fin du fichier
		if(returnString.equals("-16777216")) return null; //Si la couleur lue est noir, on retourne null
		if(AllowedInstruction.isAllowed(returnString))return returnString; //Si le résultat est reconnu comme une instruction, on le retourne
		else throw new WrongInstructionException(returnString); //Sinon on lance une erreur de lecture d'instruction
	}
}
